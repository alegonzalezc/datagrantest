from jsonschema import validate
from jsonschema.exceptions import ValidationError
from jsonschema.exceptions import SchemaError
from app import mongo


user_schema = {
    "type": "object",
    "properties": {
        "User": {
            "name": {
                "type": "string",
                "maxLength": 50
            },
            "email": {
                "type": "string",
                "format": "email",
                "maxLength": 50
            },
            "password": {
                "type": "string",
                "minLength": 5,
                "maxLength": 50
            }
        }
    },
    "required": ["User"],
    "additionalProperties": False
}


def validate_user(data):
    try:
        validate(data, user_schema)
    except ValidationError as e:
        return {'ok': False, 'message': e}
    except SchemaError as e:
        return {'ok': False, 'message': e}
    return {'ok': True, 'data': data}


class User(mongo.Document):
    email = mongo.StringField(required=True)
    name = mongo.StringField(max_length=50, required=True)
    password = mongo.StringField(max_length=200, min_length=5, required=True)
