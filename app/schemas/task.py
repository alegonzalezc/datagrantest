
from app.schemas.user import User
from jsonschema import validate
from jsonschema.exceptions import ValidationError
from jsonschema.exceptions import SchemaError
from app import mongo


task_schema = {
    "type": "object",
    "properties": {
        "title": {
            "type": "string",
            "maxLength": 50
        },
        "author": {
            "email": {
                "type": "string",
                "format": "email",
                "maxLength": 50
            }
        },
        "done": {
            "type": "number",
            "maxLength": 1
        }
    },
    "required": ["title", "author", "done"],
    "additionalProperties": False
}


def validate_task(data):
    try:
        validate(data, task_schema)
    except ValidationError as e:
        return {'ok': False, 'message': e}
    except SchemaError as e:
        return {'ok': False, 'message': e}
    return {'ok': True, 'data': data}


class Task(mongo.Document):
    title = mongo.StringField(max_length=100, required=True)
    author = mongo.ReferenceField(User, reverse_delete_rule=mongo.CASCADE, required=True)
    done = mongo.BooleanField()
