
import os
from flask import request, jsonify
from flask_jwt_extended import (create_access_token,
                                create_refresh_token,
                                jwt_required,
                                jwt_refresh_token_required,
                                get_jwt_identity)
from app import app, flask_bcrypt, jwt, logger
from app.schemas.user import validate_user, User
from app.schemas.task import Task
from app.controllers.crossdomain import crossdomain

ROOT_PATH = os.environ.get('ROOT_PATH')
print(ROOT_PATH)
LOG = logger.get_root_logger(__name__, filename=os.path.join(ROOT_PATH, 'output.log'))


@jwt.unauthorized_loader
def unauthorized_response(callback):
    return jsonify({
        'ok': False,
        'message': 'Missing Authorization Header'
    }), 401


@app.route('/auth', methods=['POST'])
def auth_user():
    LOG.debug(request.get_json())
    data = validate_user(request.get_json())
    if data['ok']:
        data = data['data']
        results = User.objects(email=data['User']['email'])
        user = results[0] if len(results) > 0 else None
        authToken = {}
        if user and flask_bcrypt.check_password_hash(user.password, data['User']['password']):
            LOG.debug({'User': {'email': user.email, 'name': user.name}})
            access_token = create_access_token(identity=data)
            refresh_token = create_refresh_token(identity=data)
            authToken['token'] = access_token
            authToken['refresh'] = refresh_token
            tasks = Task.objects(author=user)
            tasksJson = [{"task": {"id": task.pk, "title": task.title, "done": task.done}} for task in tasks]
            return jsonify({'ok': True, "User":{"name": user.name, "email": user.email}, 'authToken': authToken, "tasks": tasksJson}), 200
        else:
            return jsonify({'ok': False, 'message': 'Invalid username or password'}), 401
    else:
        return jsonify({'ok': False, 'message': 'Bad request parameters: {}'.format(data['message'])}), 400


@app.route('/register', methods=['POST'])
def register():
    data = validate_user(request.get_json())
    if data['ok']:
        data = data['data']
        results = User.objects(email=data['User']['email'])
        user = results[0] if len(results) > 0 else None
        if not user:
            data['User']['password'] = flask_bcrypt.generate_password_hash(data['User']['password'])
            User(email=data['User']['email'], name=data['User']['name'], password=data['User']['password']).save()
            return jsonify({'ok': True, 'message': 'User created successfully!'}), 200
        else:
            return jsonify({'ok': False, 'message': 'User already registered'}), 200
    else:
        return jsonify({'ok': False, 'message': 'Bad request parameters: {}'.format(data['message'])}), 400


@app.route('/refresh-session', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    current_user = get_jwt_identity()
    ret = {
        'token': create_access_token(identity=current_user)
    }
    return jsonify({'ok': True, 'data': ret}), 200


@app.route('/user', methods=['GET', 'DELETE', 'PATCH'])
@jwt_required
def user():
    # route read user
    if request.method == 'GET':
        query = request.args
        if query['email']:
            results = User.objects(email=query['email'])
            user = results[0] if len(results) > 0 else None
            if user:
                return jsonify({'ok': True, 'user': {'email': user.email, 'name': user.name}}), 200
            else:
                return jsonify({'ok': False, 'message': 'No records found'}), 200
        else:
            return jsonify({'ok': False, 'message': 'Bad request parameters!'}), 400

    data = request.get_json()
    if request.method == 'DELETE':
        if data.get('email', None) is not None:
            results = User.objects(email=data['email'])
            user_deleted = results[0] if len(results) > 0 else None
            if user_deleted:
                user_deleted.delete()
                response = {'ok': True, 'message': 'Record deleted'}
            else:
                response = {'ok': False, 'message': 'No record found'}
            return jsonify(response), 200
        else:
            return jsonify({'ok': False, 'message': 'Bad request parameters!'}), 400

    if request.method == 'PATCH':
        if data.get('query', {}) != {} and data.get('payload', {}) != {}:
            results = User.objects(email=data["query"]["email"])
            update_user = results[0] if len(results) > 0 else None
            if update_user:
                update_user.name = data["payload"]["name"]
                update_user.email = data["payload"]["email"]
                update_user.save()
                return jsonify({'ok': True, 'message': 'Record updated'}), 200
            else:
                return jsonify({'ok': False, 'message': 'No record found'}), 200
        else:
            return jsonify({'ok': False, 'message': 'Bad request parameters!'}), 400
