import os
from flask import request, jsonify
from flask_jwt_extended import jwt_required
from app import app, jwt, logger
from app.schemas.task import validate_task, Task
from app.schemas.user import User

ROOT_PATH = os.environ.get('ROOT_PATH')
print(ROOT_PATH)
LOG = logger.get_root_logger(__name__, filename=os.path.join(ROOT_PATH, 'output.log'))


@jwt.unauthorized_loader
def unauthorized_response(callback):
    return jsonify({
        'ok': False,
        'message': 'Missing Authorization Header'
    }), 401


@app.route('/create-task', methods=['POST'])
@jwt_required
def create_task():
    data = validate_task(request.get_json())
    if data['ok']:
        data = data['data']
        LOG.debug(data)
        results = User.objects(email=data['author']['email'])
        user = results[0] if len(results) > 0 else None
        if user:
            Task(title=data['title'], author=user, done=data['done']).save()
            return jsonify({'ok': True, 'message': 'Task created successfully!'}), 200
        else:
            return jsonify({'ok': False, 'message': 'Task author not found'}), 200

    else:
        return jsonify({'ok': False, 'message': 'Bad request parameters: {}'.format(data['message'])}), 400


@app.route('/task', methods=['GET', 'DELETE', 'PATCH'])
@jwt_required
def task():
    if request.method == 'GET':
        query = request.args
        if query['email']:
            results = User.objects(email=query['email'])
            user = results[0] if len(results) > 0 else None
            if user:
                tasks = Task.objects(author=user)
                response = {"ok": True, "tasks": [{"task": {"id": task.pk, "title": task.title, "done": task.done}} for task in tasks]}
                return jsonify(response), 200
            else:
                return jsonify({'ok': False, 'message': 'No author found'}), 200
        else:
            return jsonify({'ok': False, 'message': 'Bad request parameters!'}), 400

    data = request.get_json()
    if request.method == 'DELETE':
        if data.get('pk', None) is not None:
            results = Task.objects(pk=data['pk'])
            task_deleted = results[0] if len(results) > 0 else None
            if task_deleted:
                task_deleted.delete()
                response = {'ok': True, 'message': 'Record deleted'}
            else:
                response = {'ok': False, 'message': 'No record found'}
            return jsonify(response), 200
        else:
            return jsonify({'ok': False, 'message': 'Bad request parameters!'}), 400

    if request.method == 'PATCH':
        if data.get('query', {}) != {} and data.get('payload', {}) != {}:
            results = Task.objects(pk=data['query']['pk'])
            task_update = results[0] if len(results) > 0 else None
            if task_update:
                LOG.debug({"task": {"id": task_update.pk, "title": task_update.title, "done": task_update.done}})
                task_update.title = data["payload"]["title"]
                task_update.done = data["payload"]["done"]
                task_update.save()
                return jsonify({'ok': True, 'message': 'Record updated'}), 200
            else:
                return jsonify({'ok': False, 'message': 'No record found'}), 200
        else:
            return jsonify({'ok': False, 'message': 'Bad request parameters!'}), 400
