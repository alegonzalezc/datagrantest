
import os
import sys
from flask import send_from_directory

ROOT_PATH = os.path.dirname(os.path.realpath(__file__))
os.environ.update({'ROOT_PATH': ROOT_PATH})
sys.path.append(ROOT_PATH)

PUBLIC_PATH = os.path.join(ROOT_PATH, 'publicResources')
STATIC_FOLDER = os.path.join(PUBLIC_PATH, 'static')
CSS = os.path.join(STATIC_FOLDER, 'css')
JS = os.path.join(STATIC_FOLDER, 'js')

from app import app, logger

# Create a logger object to log the info and debug
LOG = logger.get_root_logger(os.environ.get('ROOT_LOGGER', 'root'), filename=os.path.join(ROOT_PATH, 'output.log'))

# Port variable to run the server on.
PORT = os.environ.get('PORT')


@app.errorhandler(404)
def not_found(error):
    # error handler
    LOG.error(error)
    return send_from_directory(PUBLIC_PATH, 'pageNotFound.html')


@app.route('/')
def index():
    # static files serve
    return send_from_directory(PUBLIC_PATH, 'index.html')


@app.route('/<path:path>')
def static_proxy(path):
    return send_from_directory(PUBLIC_PATH, path)


@app.route('/static/css/<path:path>')
def static_proxy_css(path):
    return send_from_directory(CSS, path)


@app.route('/static/js/<path:path>')
def static_proxy_js(path):
    return send_from_directory(JS, path)


if __name__ == '__main__':
    LOG.info('running environment: %s', os.environ.get('ENV'))
    app.config['DEBUG'] = os.environ.get('ENV') == 'development'
    app.run(host='0.0.0.0', port=int(PORT))
