import React, { Component } from 'react';
import './App.css';
import 'bulma/css/bulma.css';

class Dashboard extends Component {

	state = {
	    "User": {
			"email": "",
			"name": "",
			"password": ""
	    },
	    "authToken": {
			"refresh": null,
			"token": null
	    },
	    "myTodoList": {
			"tasks": []
	    },
	    "newTask":{
			"title": ""
	    }
  };

  	render(){
  		console.log("RenderTable");
  		console.log(this.props.data);
	    return this.props.data.myTodoList.tasks.map((item, index) => (
	    	<a className="panel-block is-active">
		    	<label className="checkbox mtl-dashboard-row-column">
				  <input type="checkbox" value={item.task.done} />
				</label>
		    	<p className="mtl-dashboard-row-column" >{item.task.title}</p>
			</a>
		));
  	}
}

export default Dashboard;
