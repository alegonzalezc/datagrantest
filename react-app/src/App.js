import React, { Component } from 'react';
import './App.css';
import 'bulma/css/bulma.css';
import './myTodoList.css';
import * as $ from 'jquery';


class App extends Component {

  state = {
    "User": {
      "email": "",
      "name": "",
      "password": ""
    },
    "authToken": {
      "refresh": null,
      "token": null
    },
    "userTasks": [],
    "newTask":{
      "title": ""
    }
  };

  handleSetEmail = (event) => {
    var newState = this.state;
    newState.User.email = event.target.value;
    this.setState(newState);
  }

  handleSetName = (event) => {
    var newState = this.state;
    newState.User.name = event.target.value;
    this.setState(newState);
  }

  handleSetPassword = (event) => {
    var newState = this.state;
    newState.User.password = event.target.value;
    this.setState(newState);
  }

  handleSetNewTaskTitle = (event) => {
    var newState = this.state;
    newState.newTask.title = event.target.value;
    this.setState(newState);
  }

  authenticateUser = (event) => {
    if(this.state.User.email === "" | this.state.User.password === ""){
      alert("Please, email and password are required");
    }
    else{
      var url = 'http://localhost:4000/auth';
      fetch( url, 
        {
          method: 'POST',
          headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({User:this.state.User})
        }
      ).then((response) => {
        var json=null;
        try{
          json=response.json();
        }catch(error){
          console.error(error);
        }
        return json;
      }).then((resp) => {
        if(resp!=null){
          if (resp.ok){
            var newState = this.state;
            newState.authToken=resp.authToken;
            newState.userTasks = resp.tasks;
            newState.User = resp.User;
            this.setState(newState);
          }else{
            alert(resp.message)
          }

        }else{
          alert('Error. Operation failure');
        }
        
      });
    }
  }

  registerUser = (event) => {
    if(this.state.User.email === "" | this.state.User.password === "" | this.state.User.name===""){
      alert("Please, email, name and password are required");
    }
    else{
      var url = 'http://localhost:4000/register';
      fetch( url, 
        {
          method: 'POST',
          headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({User:this.state.User})
        }
      ).then((response) => {
        var json=null;
        try{
          json=response.json();
        }catch(error){
          console.error(error);
        }
        return json;
      }).then((resp) => {
        if(resp!=null){
          if (resp.ok){
            this.changeToLoginTab(null);
          }
          alert(resp.message);

        }else{
          alert('Error. Operation failure');
        }
        
      });
    }
  }

  createNewTask = (event) => {
    if(this.state.newTask.title===""){
      alert("Please title task is required");
    }
    else{
      var url = 'http://localhost:4000/create-task';
      fetch( url, 
        {
          method: 'POST',
          headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+this.state.authToken.token
          },
          body: JSON.stringify({"title":this.state.newTask.title, "author":{"email":this.state.User.email}, "done": 0})
        }
      ).then((response) => {
        var json=null;
        try{
          json=response.json();
        }catch(error){
          console.error(error);
        }
        return json;
      }).then((resp) => {
        if(resp!=null){
          if (resp.ok){
            this.refreshTasks(event);
          }else{
            alert(resp.message);
          }
        }else{
          alert('Error. Operation failure');
        }
        var newState = this.state;
        newState.newTask.title = "";
        this.setState(newState);
      });
    }
  }


  refreshTasks = (event) =>{
    if(this.state.User.email !== ""){
      var url = 'http://localhost:4000/task?email='+this.state.User.email;
      fetch( url, 
        {
          method: 'GET',
          headers:{
            'Accept': 'application/json',
            'Authorization': 'Bearer '+this.state.authToken.token
          }
        }
      ).then((response) => {
        var json=null;
        try{
          json=response.json();
        }catch(error){
          console.error(error);
        }
        return json;
      }).then((resp) => {
        if(resp!=null){
          if (resp.ok){
            var newState = this.state;
            newState.userTasks = resp.tasks;
            this.setState(newState);
          }else{
            alert(resp.message)
          }

        }else{
          alert('Error. Operation failure');
        }
        
      });
    }
  }

  setTasksValues = (event)=>{
    var tasks = $('input[id*="mtl-row-"]');
    var taskDone;

    for (var i=0; i < this.state.userTasks.length; ++i) {
      var taskId = tasks[i].id.replace("mtl-row-", "");
      taskDone = tasks[i].checked;

      if (taskId===this.state.userTasks[i].task.id) {
        if(taskDone!==this.state.userTasks[i].task.done){
          console.log("update task-"+taskId);
          this.updateTask({"id": taskId, "title":this.state.userTasks[i].task.title, "done": taskDone});
          var newState = this.state;
          newState.userTasks[i].task.done = taskDone;
          this.setState(newState);
          console.log("cerrar loop1");
          console.log(this.state);
          $('input#mtl-row-'+taskId).checked=taskDone;
          break;
        }
      }
    }
  }

  updateTask(task) {
    if(task == null){
      alert("Error. Operation failure");
    }
    else{
      var url = 'http://localhost:4000/task';
      fetch( url, 
        {
          method: 'PATCH',
          headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+this.state.authToken.token
          },
          body: JSON.stringify({"query":{"pk":task.id}, "payload":{"title": task.title, "done": task.done}})
        }
      ).then((response) => {
        var json=null;
        try{
          json=response.json();
        }catch(error){
          console.error(error);
        }
        return json;
      }).then((resp) => {
        if(resp!=null){
          if (!resp.ok){
            alert(resp.message);
          }
        }else{
          alert('Error. Operation failure');
        }
      });
    }
  }

  logOutUser = (event) => {
    var newState = this.state;
    newState.authToken.token=null;
    newState.authToken.refresh=null;
    this.setState(newState);
  }

  changeToLoginTab = (event) =>{
    $('#registerPanelTab').removeClass('is-active');
    $('#loginPanelTab').addClass('is-active');
    $('#registerPanel').addClass('mtl-hidden');
    $('#loginPanel').removeClass('mtl-hidden');
  }

  changeToRegisterTab = (event) =>{
    $('#registerPanelTab').addClass('is-active');
    $('#loginPanelTab').removeClass('is-active');
    $('#registerPanel').removeClass('mtl-hidden');
    $('#loginPanel').addClass('mtl-hidden');
  }


  render() {
    var tasksHtml = [];
    for(var i=0; i< this.state.userTasks.length; i++){
      tasksHtml.push(
        <div className="panel-block is-active">
          <label className="checkbox mtl-dashboard-row-column">
            <input id={'mtl-row-'+this.state.userTasks[i].task.id} checked={this.state.userTasks[i].task.done} onChange={this.setTasksValues} type="checkbox" />
          </label>
          <p className="mtl-dashboard-row-column" >{this.state.userTasks[i].task.title}</p>
        </div>
      );
    }
    return (
      this.state.authToken.token == null ?
        <div className="App">
          <div className="container">
            <div className="section">
                <h1 className="title is-1 mtl-title">My TODO List</h1>
                  <div className="box mtl-login-container">
                      <div className="tabs">
                        <ul>
                          <li id="loginPanelTab" className="is-active" onClick={this.changeToLoginTab}><a>Log in</a></li>
                          <li id="registerPanelTab" onClick={this.changeToRegisterTab} ><a>Sign up</a></li>
                        </ul>
                      </div>
                      <div id="loginPanel" className="card-content">
                            <div className="field">
                              <label className="label">Email</label>
                              <div className="control">
                                <input className="input" type="email" placeholder="Email input" onChange={this.handleSetEmail} />
                              </div>
                            </div>

                            <div className="field">
                              <label className="label">Password</label>
                              <div className="control">
                                <input className="input" type="password" placeholder="Password input" onChange={this.handleSetPassword} />
                              </div>
                            </div>
                            <button className="button is-link is-rounded" onClick={this.authenticateUser} >Log in</button>
                      </div>
                      <div id="registerPanel" className="card-content mtl-hidden">
                            <div className="field">
                              <label className="label">Name</label>
                              <div className="control">
                                <input className="input" type="text" placeholder="Text input" onChange={this.handleSetName} />
                              </div>
                            </div>

                            <div className="field">
                              <label className="label">Email</label>
                              <div className="control">
                                <input className="input" type="email" placeholder="Email input" onChange={this.handleSetEmail} />
                              </div>
                            </div>

                            <div className="field">
                              <label className="label">Password</label>
                              <div className="control">
                                <input className="input" type="password" placeholder="Password input" onChange={this.handleSetPassword} />
                              </div>
                            </div>

                            <button className="button is-success is-rounded" onClick={this.registerUser}>Sing up</button>
                      </div>
                  </div>
            </div>
          </div>

          <footer className="footer">
              <div className="container">
                  <div className="content has-text-centered">
                      <p>
                          <strong>MyTodoList</strong> by Alexander Gonzalez.
                      </p>
                  </div>
              </div>
          </footer>
        </div>
      :
        <div className="App">
            <nav className="navbar has-background-grey-light">
                <div className="container">
                      <div className="navbar-brand">
                          <h1 className="title is-3 has-text-warning">My TODO List</h1>
                      </div>
                      <div id="navbarMenu" className="navbar-menu">
                          <div className="navbar-end">
                              <div className="navbar-item has-dropdown is-hoverable">
                                  <a className="navbar-link">
                                      Account
                                  </a>
                                  <div className="navbar-dropdown">
                                      <div className="navbar-item">
                                          {this.state.User.name}
                                      </div>
                                      <hr className="navbar-divider" />
                                      <a className="navbar-item" onClick={this.logOutUser} >
                                          Logout
                                      </a>
                                  </div>
                              </div>
                          </div>
                      </div>
                </div>
            </nav>
            <div className="container">
              <div className="section">
                  <div className="box mtl-dashboard-container">
                      <nav id="mtl-task-table" className="panel">
                        <p className="panel-heading">
                          My tasks
                        </p>
                        <div className="panel-block">
                          <p className="mtl-dashboard-table-column">Is it done?</p>
                          <p className="mtl-dashboard-table-column" >Title</p> 
                        </div>
                        {tasksHtml}
                        <a className="panel-block is-active">
                          <input className="input mtl-new-task-field" type="text" placeholder="new task" onChange={this.handleSetNewTaskTitle}/>
                          <button className="button is-success is-rounded mtl-new-task-field" onClick={this.createNewTask} >Create</button> 
                        </a>
                      </nav>
                  </div>
              </div>
            </div>

            <footer className="footer">
                <div className="container">
                    <div className="content has-text-centered">
                        <p>
                            <strong>MyTodoList</strong> by Alexander Gonzalez.
                        </p>
                    </div>
                </div>
            </footer>
          </div>
    );
  }

}

export default App;
