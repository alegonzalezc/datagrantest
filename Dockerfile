FROM python:3.7
ADD . /usr/myTodoList
WORKDIR /usr/myTodoList
RUN pip install --upgrade pip
RUN pip install -r dependencies.txt
COPY . .
EXPOSE 4000
ENTRYPOINT ["python","app.py"] 